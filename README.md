# test-python

Test Python contains all the boilerplate you need to create a Python package.


* Free software: Apache Software License 2.0.2

Features
--------

* TODO

Credits
-------

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter) and the [`audreyr/cookiecutter-pypackage`](https://github.com/audreyr/cookiecutter-pypackage) project template.
